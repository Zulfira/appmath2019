package obrv;

public class A {

	public static void main(String[] args) {

		C b = new C(new B());
		b.cl();
	}
}

class B implements E {

	public int ex(int a, int b) {
		int c = a + b;

		System.out.println(c);
		return c;
	}
}

interface E {
	int ex(int a, int b);
}

class C {
	int f;
	E handler;

	C(E action) {

		this.handler = action;
	}

	public void cl() {

		f = handler.ex(5, 4) * 4;
	System.out.println(f);
	}
}
